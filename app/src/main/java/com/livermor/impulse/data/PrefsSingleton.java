package com.livermor.impulse.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.livermor.impulse.ApplicationClass;

/**
 * Created by arturdumchev on 04.11.15.
 */
public class PrefsSingleton {
    //name
    public static final String MAIN = "com.livermor.slaughterhouse";

    private static Context mContext;
    private static SharedPreferences mPrefs;

    private static volatile PrefsSingleton instance;
    private PrefsSingleton() {
        mContext = ApplicationClass.getContext();
    }

    public static PrefsSingleton getInstance() {
        if (instance == null) {
            synchronized (PrefsSingleton.class) {
                if (instance == null) {
                    instance = new PrefsSingleton();
                    mPrefs = mContext.getSharedPreferences(MAIN, mContext.MODE_PRIVATE);
                }
            }
        }
        return instance;
    }

    public static SharedPreferences getSharedPrefs() {
        return mPrefs;
    }

    //first visit
    public static final String FIRST_RUN = "firstrun";

    //modes
    public static final String SOUND_ON = "soundMode";

    //rate this app
    public static final String RATING_TIME = "ratingTime";

    //
    public static final String RECORD = "userRecord";

    //
    public static final String HERO_TYPE = "currentHero";
}
