package com.livermor.impulse.data;

import android.util.Log;

import com.jawnnypoo.physicslayout.PhysicsRelativeLayout;
import com.livermor.impulse.managers.Units;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

/**
 * Remember current game state
 */
public class SessionSingleton {

    private static final String TAG = SessionSingleton.class.getSimpleName();
    private static volatile SessionSingleton instance;

    private volatile byte currentLevel = -1;
    private volatile short enemiesKilled = 0;
    private volatile byte enemyBooster = 0;
    private volatile boolean heroIsDead = false;
    //private volatile short sessionNumber = 1;
    private volatile boolean pause = false;
    private volatile short heroSpeed = 100;
    private volatile short heroImpulseOnEnemy = 0;

    private SessionSingleton() {
        //do something
    }

    //single instance
    public static SessionSingleton getInstance() {
        if (instance == null) {
            synchronized (SessionSingleton.class) {
                if (instance == null) {
                    instance = new SessionSingleton();
                }
            }
        }
        return instance;
    }

    //Level
    //**********************************
    public byte getCurrentLevel() {
        return currentLevel;
    }

    public void increaseCurrentLevel() {
        currentLevel++;
    }

    public void resetCurrentLevel() {
        currentLevel = 1;
    }

    public void setCurrentLevel(byte currentLevel) {
        this.currentLevel = currentLevel;
    }

    //Enemies killed
    //**********************************
    public short getEnemiesKilled() {
        return enemiesKilled;
    }

    public void resetEnemiesKilled() {
        enemiesKilled = 0;
    }

    public void increaseEnemiesKilled() {
        enemiesKilled++;
    }

    public void setEnemiesKilled(short enemiesKilled) {
        this.enemiesKilled = enemiesKilled;
    }

    //Enemy booster
    //**********************************
    public void increaseEnemyBooster() {

        if (currentLevel < 5) {
            enemyBooster += 3;
        } else if (currentLevel < 10) {
            enemyBooster += 2;
        } else {
            enemyBooster += 1;
        }

        heroImpulseOnEnemy -= heroImpulseOnEnemy / 2 / currentLevel;
    }

    public void resetBooster() {
        enemyBooster = 0;
        heroImpulseOnEnemy =
                (short) (150  + heroSpeed / 5);
    }

    public byte getEnemyBooster() {
        return enemyBooster;
    }


    //hero props
    //**********************************
    public void makeHeroAlive() {
        heroIsDead = false;
    }

    public void killHero() {
        heroIsDead = true;
    }

    public boolean heroIsDead() {
        return heroIsDead;
    }

    public void setHeroSpeed(short heroSpeed) {
        this.heroSpeed = heroSpeed;
    }

    //synchronized
    // Main level proprieties:
    // create/destroy views, start levels
    //********************************************************************
    public synchronized void nextLevel(
            PhysicsRelativeLayout mPhysicsLayout,
            Units u) {

      //  Log.i("This is fucking", " SYNCHRONIZE OR NOT");

        //sessionNumber++;
        mPhysicsLayout.removeAllViews();
        increaseEnemyBooster();
        increaseCurrentLevel();
        resetEnemiesKilled();
        u.getHero();

        if (currentLevel % 5 == 0) {
            u.getBoss();
        } else {
            u.getEnemies(currentLevel);
        }
    }

    public synchronized void firstLevel(
            PhysicsRelativeLayout mPhysicsLayout,
            Units u) {

//        sessionNumber++;
        mPhysicsLayout.removeAllViews();
        resetBooster();
        resetEnemiesKilled();
        resetCurrentLevel();
        makeHeroAlive();
        u.getHero();
        u.getEnemies(currentLevel);
    }

    public synchronized void enemyAttack(
            int yourLevelNumber,
            PhysicsRelativeLayout mPhysicsLayout,
            int position) {

        // Log.i("EnemyAttack", "I am alive");

        if (yourLevelNumber != currentLevel || pause || mPhysicsLayout.getChildAt(position) == null) {
            //Log.i("EnemyAttack", " FALSE SESSION NUMBER");

            return;
        }

        //getCurrent body
        Body enemy = (Body) mPhysicsLayout.getChildAt(position).getTag(
                com.jawnnypoo.physicslayout.R.id.physics_layout_body_tag
        );

        //calculate the vector
        Body hero = (Body) mPhysicsLayout.getChildAt(0).getTag(
                com.jawnnypoo.physicslayout.R.id.physics_layout_body_tag
        );

        if (mPhysicsLayout.getChildAt(position) == null || mPhysicsLayout.getChildAt(0) == null) {
  //          Log.i("EnemyAttack", "SOMEWHY IT IS NULL!!!!!");
            return;
        }

        if (currentLevel == -1 || hero == null || enemy == null) {
  //          Log.i(TAG, "currentLevel == -1||hero==null||enemy==null");
            return;
        }
        Vec2 impulse = new Vec2(
                hero.getPosition().x - enemy.getPosition().x,
                hero.getPosition().y - enemy.getPosition().y
        );
        //add power to vector
        impulse.normalize();
        impulse = impulse.mul(200f);

        enemy.applyLinearImpulse(impulse, enemy.getPosition());
    }

    public synchronized void heroAttack(
            PhysicsRelativeLayout mPhysicsLayout,
            float x,
            float y
    ) {

        Body hero;
        Vec2 impulse;

        if (mPhysicsLayout.getChildAt(0) != null) {
            hero = (Body) mPhysicsLayout.getChildAt(0).getTag(
                    com.jawnnypoo.physicslayout.R.id.physics_layout_body_tag
            );

            if (hero == null) return;
            impulse = new Vec2(
                    x - hero.getPosition().x,
                    y - hero.getPosition().y
            );
            impulse.normalize();
            impulse = impulse.mul(heroSpeed);

            hero.applyLinearImpulse(impulse, hero.getPosition());

            //if (mPhysicsLayout.getChildCount() == 0) return;
            Body body;

            impulse.normalize();
            impulse = impulse.mul(heroImpulseOnEnemy);


            byte increment = (byte) ((currentLevel - enemiesKilled) <= 2? 1:2);

            for (byte i = 1; i < mPhysicsLayout.getChildCount(); i += increment) {
                //help impulse for hero

                if (mPhysicsLayout.getChildAt(i) == null) continue;
                body = (Body) mPhysicsLayout.getChildAt(i).getTag(
                        com.jawnnypoo.physicslayout.R.id.physics_layout_body_tag);
                body.applyLinearImpulse(impulse, body.getPosition());
            }

     //       Log.i(TAG, "impulse is " + impulse.length());
        }
    }


    // Pause
    //**********************************
    public boolean isPause() {
        return pause;
    }

    public void setPause(boolean pause) {
        this.pause = pause;
    }
}
