package com.livermor.impulse.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jpardogo.listbuddies.lib.adapters.CircularLoopAdapter;
import com.livermor.impulse.R;
import com.livermor.impulse.ui.ScaleToFitWidhtHeigthTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arturdumchev on 03.11.15.
 */
public class CircularAdapter extends CircularLoopAdapter {

    private List<Integer> mItems = new ArrayList<>();
    private Context mContext;
    private int mRowHeight;

    public CircularAdapter(Context context, int rowHeight, List<Integer> imagesUrl) {
        mContext = context;
        mRowHeight = rowHeight;
        mItems = imagesUrl;
    }

    @Override
    public Integer getItem(int position) {
        return mItems.get(getCircularPosition(position));
    }

    @Override
    protected int getCircularCount() {
        return mItems.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.image.setMinimumHeight(mRowHeight);

        Picasso.with(mContext)
                .load(getItem(position))
                .transform(new ScaleToFitWidhtHeigthTransform(mRowHeight, true))
                .into(holder.image);

        return convertView;
    }


    static class ViewHolder {
        ImageView image;

        public ViewHolder(View convertView) {
            image = (ImageView) convertView.findViewById(R.id.item_image);
        }
    }
}
