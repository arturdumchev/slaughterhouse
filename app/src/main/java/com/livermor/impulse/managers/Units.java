package com.livermor.impulse.managers;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.view.Gravity;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jawnnypoo.physicslayout.Physics;
import com.jawnnypoo.physicslayout.PhysicsConfig;
import com.jawnnypoo.physicslayout.PhysicsRelativeLayout;
import com.livermor.impulse.R;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.data.SessionSingleton;

import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by arturdumchev on 01.11.15.
 */
public class Units {

    private static final String TAG = Units.class.getSimpleName();

    Activity mActivity;
    volatile PhysicsRelativeLayout mPhysicsLayout;
    Rect mArea;
    Random rand;
    private static SharedPreferences mPrefs;
    private static SessionSingleton mSession;

    public Units(Activity a, PhysicsRelativeLayout ph, Rect r) {
        mActivity = a;
        mPhysicsLayout = ph;
        mArea = r;
        mPrefs = PrefsSingleton.getSharedPrefs();
        rand = new Random();
        mSession = SessionSingleton.getInstance();
    }

    /**********************************************************
     * get aggressive enemies
     *********************************************************/
    public void getEnemies(int i) {

        short boost = mSession.getEnemyBooster();
        short reflectionTime = (short) (
                mSession.getCurrentLevel() * (500));

        while (i > 0) {
            i--;
            int position = mSession.getCurrentLevel() - i;
            createEnemy(
                    position,
                    boost,
                    false,
                    reflectionTime,
                    R.drawable.agent_smith
            );
        }
    }

    /**********************************************************
     * get Boss —  sizeBooster increased
     *********************************************************/
    public void getBoss() {

        byte bossBoost = 10;
        short boost = (short) (bossBoost + mSession.getEnemyBooster());

        createEnemy(1,
                boost,
                true,
                (short) 1500,
                R.drawable.agent_smith);
    }

    /**********************************************************
     * get Hero
     *********************************************************/
    public void getHero() {
        //create hero
        final CircleImageView hero = new CircleImageView(mActivity);
        final SessionSingleton mSession = SessionSingleton.getInstance();

        int heroChoice = mPrefs.getInt(PrefsSingleton.HERO_TYPE, 0);
        LinearLayout.LayoutParams llp = null;

        if (heroChoice == 0) {
            hero.setImageResource(R.drawable.hero_neo);
            llp = new LinearLayout.LayoutParams(
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size),
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size)
            );
            mSession.setHeroSpeed((short) 270);

        } else if (heroChoice == 1) {
            hero.setImageResource(R.drawable.hero_morph);
            llp = new LinearLayout.LayoutParams(
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) + 10,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) + 10
            );
            mSession.setHeroSpeed((short) 150);

        } else if (heroChoice == 2) {
            hero.setImageResource(R.drawable.hero_trin);
            llp = new LinearLayout.LayoutParams(
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) - 3,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) - 3
            );
            mSession.setHeroSpeed((short) 400);
        }
        hero.setAlpha(0.90f);
        hero.setBorderWidth(3);
        hero.setBorderColor(
                mActivity.getResources().getColor(R.color.textColor)
        );

        llp.gravity = Gravity.CENTER;
        hero.setLayoutParams(llp);
        mPhysicsLayout.addView(hero);
        PhysicsConfig config = new PhysicsConfig.Builder()
                .setShapeType(PhysicsConfig.ShapeType.CIRCLE)
                .setDensity(0.9f)
                .setFriction(0.9f)
                .setRestitution(0.4f)
                .build();
        Physics.setPhysicsConfig(hero, config);

        final ViewTreeObserver vto = hero.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                if (!hero.getLocalVisibleRect(mArea)) {

                    mSession.killHero();
                    hero.getViewTreeObserver()
                            .removeOnPreDrawListener(this);
                }

                return true;
            }
        });

    }

    /*********************************************
     * get harmless enemies
     *********************************************/
    public void getPunchingBags() {


        final SessionSingleton mSession = SessionSingleton.getInstance();
        int i = 1;

        while (i > 0) {
            i--;
            //create enemy
            final CircleImageView enemy = new CircleImageView(mActivity);
            enemy.setImageResource(R.drawable.hero_morph);

            int booster = mSession.getEnemyBooster();
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) + 10,
                    mActivity.getResources().getDimensionPixelSize(R.dimen.hero_size) + 10
            );
            enemy.setLayoutParams(llp);
            mPhysicsLayout.addView(enemy);
            PhysicsConfig config = new PhysicsConfig.Builder()
                    .setShapeType(PhysicsConfig.ShapeType.CIRCLE)
                    .setDensity(0.6f)
                    .setFriction(0.4f)
                    .setRestitution(0.4f)
                    .build();
            Physics.setPhysicsConfig(enemy, config);

            enemy.setAlpha(0.83f);
            enemy.setBorderWidth(3);
            enemy.setBorderColor(
                    mActivity.getResources().getColor(R.color.textColor)
            );

            //watch for enemy
            final ViewTreeObserver vto = enemy.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {

                    if (!enemy.getLocalVisibleRect(mArea)) {

                        mSession.increaseEnemiesKilled();
                        enemy.getViewTreeObserver()
                                .removeOnPreDrawListener(this);
                    }

                    return true;
                }
            });
        }
    }


    //******************************************************************************************
    // INNER HELP FUNCTIONS

    private void createEnemy(
            final int position,
            short booster,
            boolean isBoss,
            final short reflectionTime,
            int icon) {
        //create enemy
        final CircleImageView enemy = new CircleImageView(mActivity);
        enemy.setImageResource(icon);
        enemy.setAlpha(0.90f);

        //enemy.setBorderOverlay(true);
        enemy.setBorderWidth(3);
        enemy.setBorderColor(
                mActivity.getResources().getColor(R.color.white)
        );


        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
                mActivity.getResources().getDimensionPixelSize(R.dimen.enemy_size) + booster,
                mActivity.getResources().getDimensionPixelSize(R.dimen.enemy_size) + booster
        );
        enemy.setLayoutParams(llp);
        mPhysicsLayout.addView(enemy);
        PhysicsConfig config = new PhysicsConfig.Builder()
                .setShapeType(PhysicsConfig.ShapeType.CIRCLE)
                .setDensity(0.6f)
                .setFriction(0.4f)
                .setRestitution(0.4f)
                .build();
        Physics.setPhysicsConfig(enemy, config);

        final int levelNumber = mSession.getCurrentLevel();

        final Thread enemyAI = new Thread(new Runnable() {

            @Override
            public void run() {
                /*Log.i(TAG, "this thread level Number = " + levelNumber);
                Log.i(TAG, "current level number = " + mSession.getCurrentLevel());*/
                while (levelNumber == mSession.getCurrentLevel() &&
                        !Thread.currentThread().isInterrupted()) {

                    //Log.i(TAG, "While  loop is working");

                    try {
                        Thread.sleep(rand.nextInt(reflectionTime)+1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mSession.enemyAttack(levelNumber, mPhysicsLayout, position);
                }
            }

        });
        enemyAI.start();

        //watch for enemy
        watchForEnemy(enemy, enemyAI, isBoss);
    }

    private void watchForEnemy(
            final ImageView enemy,
            final Thread enemyAI,
            final boolean boss
    ) {
        final ViewTreeObserver vto = enemy.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {

                if (!enemy.getLocalVisibleRect(mArea)) {

                    enemyAI.interrupt();
                    //Log.i(TAG, "enemy dead");
                    if (boss) {
                        mSession.setEnemiesKilled(
                                mSession.getCurrentLevel());
                    } else {
                        mSession.increaseEnemiesKilled();
                    }

                    enemy.getViewTreeObserver()
                            .removeOnPreDrawListener(this);
                }

                return true;
            }
        });
    }


}
