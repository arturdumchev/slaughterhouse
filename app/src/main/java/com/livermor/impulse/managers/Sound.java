package com.livermor.impulse.managers;

import android.content.Context;
import android.media.MediaPlayer;


import com.livermor.impulse.R;

import java.util.Random;

/**
 * Created by arturdumchev on 06.10.15.
 */
public class Sound {

    private static boolean isMenuPlaying;
    private static int[] menuSounds =
            {
                    R.raw.contacted_certain_indi, R.raw.future_owr, R.raw.hate_this, //3
                    R.raw.human_disease, R.raw.human_disease, R.raw.lacked_the_lang,//6
                    R.raw.marvel, R.raw.perfect, R.raw.misery, R.raw.zoo_prison,//10
                    R.raw.no_one_accept_program//11 length
            };
    private static volatile byte nextMenuPhrase = 0;

    private static int[] lostSounds =
            {
                    R.raw.lost_billions_of_people, R.raw.lost_disaster,
                    R.raw.lost_goodbue, R.raw.lost_sound_of_inevitability,
                    R.raw.lost_you_had_ur_time
                    // 5 length
            };
    private static volatile byte nextLostPhrase = 0;

    public static void configurateSession() {
        byte unpredictability =
                (byte) new Random().nextInt(5);
        nextMenuPhrase = unpredictability;
        nextLostPhrase = unpredictability;
    }

    public static void playMenuSound(Context mContext) {

        if (isMenuPlaying) return;
        isMenuPlaying = true;

        MediaPlayer mp = MediaPlayer.create(
                mContext,
                getSound(nextMenuPhrase == menuSounds.length - 1 ? 0 : nextMenuPhrase++)
        );
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                isMenuPlaying = false;
                mp.release();
            }
        });
    }

    public static void playLostSound(Context mContext) {

        if (isMenuPlaying) return;

        byte soundNumb =
                nextLostPhrase == lostSounds.length - 1 ?
                        0 : nextLostPhrase++;

        MediaPlayer mp = MediaPlayer.create(
                mContext,
                getSound((byte) (menuSounds.length + soundNumb))
        );

        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
    }

    public static void playParticularSound(Context mContext, int sound) {

        MediaPlayer mp = MediaPlayer.create(
                mContext,
                sound
        );
        mp.start();
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.release();
            }
        });
    }

    private static int getSound(byte sound) {

        switch ((int) sound) {
            case 0:
                return menuSounds[0];
            case 1:
                return menuSounds[1];
            case 2:
                return menuSounds[2];
            case 3:
                return menuSounds[3];
            case 4:
                return menuSounds[4];
            case 5:
                return menuSounds[5];
            case 6:
                return menuSounds[6];
            case 7:
                return menuSounds[7];
            case 8:
                return menuSounds[8];
            case 9:
                return menuSounds[9];
            case 10:
                return menuSounds[10];

            //lost sounds
            case 11:
                return lostSounds[0];
            case 12:
                return lostSounds[1];
            case 13:
                return lostSounds[2];
            case 14:
                return lostSounds[3];
            case 15:
                return lostSounds[4];


        }

        return R.raw.smith_agent_smith;
    }
}