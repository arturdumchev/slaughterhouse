package com.livermor.impulse.managers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.livermor.impulse.R;
import com.livermor.impulse.ui.Gamer;

/**
 * Created by arturdumchev on 05.11.15.
 */
public class ShareResult {

    private static String mShareText;
    private static ShareLinkContent mContent;
    private static Gamer mGameActivity;

    /******************************************************************************************
    * Share dialog, require activity with Gamer interface
    *******************************************************************************************/
    public static void showDialog(final Gamer gameActivity, String title, String shareText) {

        mShareText = shareText;
        mGameActivity = gameActivity;
        final Activity mActivity = (Activity) gameActivity;
        mGameActivity.pause();

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);

        if (title != null) builder.setTitle(title);
        builder.setPositiveButton(mActivity.getString(R.string.share_choice_facebook), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mGameActivity.resume();

                mContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse(getLink(mActivity)))
                        .setContentTitle(mActivity.getString(R.string.app_name))
                        .setContentDescription(mShareText)
                        .build();

                ShareDialog.show(mActivity, mContent);
            }
        });
        builder.setNegativeButton(mActivity.getString(R.string.share_choice_other), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mGameActivity.resume();

                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, mShareText + "\n" + getLink(mActivity));
                Intent chosenIntent = Intent.createChooser(intent, null);
                mActivity.startActivity(chosenIntent);
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mGameActivity.resume();
            }
        });

        builder.show();

    }

    public static String getLink(Context context) {
        String packageName = context.getPackageName();
        return "http://play.google.com/store/apps/details?id=" + packageName;
    }
}

