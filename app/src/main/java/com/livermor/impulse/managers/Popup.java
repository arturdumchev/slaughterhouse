package com.livermor.impulse.managers;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.livermor.impulse.R;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.ui.HeroActivity;
import com.livermor.impulse.ui.Training;

/**
 * Created by arturdumchev on 05.11.15.
 */
public class Popup {

    private static final String TAG = Popup.class.getSimpleName();

    private static long timeToAskForRate = 1000 * 60 * 60 * 24;//ask after one day
    private static long rateSecondAttempt = 1000 * 60 * 60 * 24 * 10;//if don't like the app
    private static long rateSecondAttempt2 = 1000 * 60 * 60 * 24 * 20;//if don't agree to rate
    private static long never = 1000 * 60 * 60 * 24 * 999l; // if already vote

    private static void ratePopup(final Activity mActivity) {

        final SharedPreferences mPrefs = PrefsSingleton.getSharedPrefs();

        long now = System.currentTimeMillis();
        long timeToRate = mPrefs.getLong(PrefsSingleton.RATING_TIME, now + timeToAskForRate);



        //false first time
        if (timeToRate >= now) {

            // Log.w(TAG, " don't show now!");

            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
        builder.setTitle(mActivity.getString(R.string.rate_title_first));

        builder.setPositiveButton(mActivity.getString(R.string.answer_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                AlertDialog.Builder builder
                        = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
                builder.setTitle(mActivity.getString(R.string.rate_title_second));

                builder.setPositiveButton(mActivity.getString(R.string.rate_answer_agree), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //never ask again
                        mPrefs.edit().putLong(PrefsSingleton.RATING_TIME, System.currentTimeMillis() + never).apply();
                        String packageName = mActivity.getPackageName();

                        Uri uri = Uri.parse(mActivity.getString(R.string.link_google_market) + packageName);
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                        try {
                            mActivity.startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            mActivity.startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(mActivity.getString(R.string.link_google_play) + packageName)));
                        }
                    }
                });

                builder.setNegativeButton(mActivity.getString(R.string.answer_negative), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //ask again after 10 days
                        mPrefs.edit().putLong(PrefsSingleton.RATING_TIME, System.currentTimeMillis() + rateSecondAttempt2).apply();
                    }
                });
                builder.show();
            }
        });
        //question was: Do you like this app?
        builder.setNegativeButton(R.string.answer_negative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //ask again after 20 days
                long rateNextTime = System.currentTimeMillis() + rateSecondAttempt;

                mPrefs.edit().putLong(PrefsSingleton.RATING_TIME, rateNextTime).apply();
            }
        });
        builder.show();

    }


    public static void popup(final Activity mActivity) {
        final SharedPreferences mPrefs = PrefsSingleton.getSharedPrefs();

        ratePopup(mActivity);

        // if first run
        if (mPrefs.getInt(PrefsSingleton.FIRST_RUN, 0) == 0) {

            //time for asking about the app
            mPrefs.edit().putLong(
                    PrefsSingleton.RATING_TIME,
                    System.currentTimeMillis() + timeToAskForRate)
                    .apply();

            //never first run again
            mPrefs.edit().putInt(PrefsSingleton.FIRST_RUN, 1).apply();

            AlertDialog.Builder builder
                    = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
            builder.setTitle("Would you like to train?");

            builder.setPositiveButton(mActivity.getString(R.string.answer_positive), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    mActivity.startActivity(new Intent(mActivity, Training.class));

                }
            });
            builder.setNegativeButton(mActivity.getString(R.string.answer_negative), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();

        } else if (mPrefs.getInt(PrefsSingleton.FIRST_RUN, 0) == 1) {
            mPrefs.edit().putInt(PrefsSingleton.FIRST_RUN, 2).apply();

            AlertDialog.Builder builder
                    = new AlertDialog.Builder(mActivity, R.style.my_alert_dialog_style);
            builder.setTitle("Choose your player");
            builder.setPositiveButton(mActivity.getString(R.string.answer_positive), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    mActivity.startActivity(new Intent(mActivity, HeroActivity.class));

                }
            });
            builder.setNegativeButton(mActivity.getString(R.string.answer_negative), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();

        }
    }
}
