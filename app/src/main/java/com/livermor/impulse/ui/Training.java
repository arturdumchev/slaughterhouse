package com.livermor.impulse.ui;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.jawnnypoo.physicslayout.Physics;
import com.jawnnypoo.physicslayout.PhysicsRelativeLayout;
import com.livermor.impulse.R;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.data.SessionSingleton;
import com.livermor.impulse.managers.Music;
import com.livermor.impulse.managers.Units;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Training extends AppCompatActivity {

    private static final String TAG = Training.class.getSimpleName();

    @Bind(R.id.physics_layout_train)
    volatile PhysicsRelativeLayout mPhysicsLayout;
    Units mUnits;
    SessionSingleton mSession;

    SharedPreferences mPrefs;
    Handler gameWatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.train_activity);
        ButterKnife.bind(this);
        mPrefs = PrefsSingleton.getSharedPrefs();

        //populate mUnits manager
        final Rect mArea = new Rect();

        mPhysicsLayout.getHitRect(mArea);
        mUnits = new Units(this, mPhysicsLayout, mArea);

        mSession = SessionSingleton.getInstance();

        mPhysicsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                //return super.onTouchEvent(event);

                //Log.i(TAG, "" + mSession.getCurrentLevel());

                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    impulseHero(e.getX() / 100, e.getY() / 100);
                }
                return true;
            }
        });

        //allow gravity and motion
        mPhysicsLayout.getPhysics().enablePhysics();
        mPhysicsLayout.setGravity((int) Physics.MOON_GRAVITY);

        //create your hero for the first level
        createHero();
        //get enemies for first level
        startLevel();

        //watching for win/loss indicators
        gameWatcher = new Handler();
        gameWatcher.post(new Runnable() {
            @Override
            public void run() {

                //if win — show Dialog, install settings and start next lever
                if (mSession.getEnemiesKilled() == 1) {

                    mSession.resetEnemiesKilled();
                    startLevel();

                } else if (mSession.heroIsDead()) {

                    mPhysicsLayout.removeAllViews();
                    mSession.resetEnemiesKilled();
                    mSession.makeHeroAlive();

                    createHero();//first view — childAt(0)
                    startLevel();
                }

//                Log.i(TAG, "" + mPhysicsLayout.getChildCount());
                gameWatcher.postDelayed(this, 1000);
            }
        });
    }

    //create main circleImage
    private void createHero() {
        //customize YourHero
        mSession.resetBooster();
        mUnits.getHero();
    }

    //get enemies depending on current level
    private void startLevel() {
        mUnits.getPunchingBags();
    }

    public void musicChange(View view) {
        boolean soundOn = mPrefs
                .getBoolean(PrefsSingleton.SOUND_ON, true);

        if (soundOn) {
            mPrefs.edit()
                    .putBoolean(PrefsSingleton.SOUND_ON, false)
                    .apply();
            Music.pause();
        } else {
            mPrefs.edit()
                    .putBoolean(PrefsSingleton.SOUND_ON, true)
                    .apply();
            Music.start(this, Music.MUSIC_GAME);
        }
    }

    public void howToPlay(View view) {
        showTutorial();
    }

    private void showTutorial() {
        createDialog(
                false,
                R.drawable.tutorial_1
        );
    }

    private void createDialog(final boolean last, int img) {
        ImageView image = new ImageView(this);
        image.setImageResource(img);

        AlertDialog.Builder builder =
                new AlertDialog.Builder(this, R.style.my_alert_dialog_tutorial_style)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if (last) {
                                    dialog.dismiss();
                                } else {
                                    createDialog(true, R.drawable.tutorial_2);
                                }
                            }
                        }).
                        setView(image)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        if (last) {
                            dialog.dismiss();
                        } else {
                            createDialog(true, R.drawable.tutorial_2);
                        }
                    }
                });


        builder.create().show();



    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.start(this, Music.MUSIC_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.pause();
    }

    //Physics
    private void impulseHero(float x, float y) {
        mSession.heroAttack(mPhysicsLayout, x, y);
    }

    @Override
    public void onBackPressed() {

        mSession.setCurrentLevel((byte) -1);
        gameWatcher.removeCallbacksAndMessages(null);
        super.onBackPressed();
    }
}
