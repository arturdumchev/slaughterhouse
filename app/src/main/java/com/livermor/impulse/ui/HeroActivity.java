package com.livermor.impulse.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.livermor.impulse.R;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.managers.Music;
import com.livermor.impulse.managers.Sound;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class HeroActivity extends AppCompatActivity {

    private SharedPreferences mPrefs;
    @Bind(R.id.circleNeo)
    CircleImageView neoCircle;
    @Bind(R.id.circleMorph)
    CircleImageView morphCircle;
    @Bind(R.id.circleTrin)
    CircleImageView trinCircle;

    private boolean morph;
    private boolean trinity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hero_content);
        ButterKnife.bind(this);

        mPrefs = PrefsSingleton.getSharedPrefs();
        morph = mPrefs.getInt(PrefsSingleton.RECORD, 1) >= 11;
        trinity = mPrefs.getInt(PrefsSingleton.RECORD, 1) >= 17;

        if (!morph) {
            morphCircle.setAlpha((float) 0.5);
        }

        if (!trinity) {
            trinCircle.setAlpha((float) 0.5);
        }


    }

    public void chooseNeo(View view) {
        mPrefs.edit().putInt(
                PrefsSingleton.HERO_TYPE,
                0
        ).apply();
        Sound.playParticularSound(this, R.raw.choose_neo);
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void chooseMorph(View view) {
        if (!morph) return;
        Sound.playParticularSound(this, R.raw.choose_morpheus);
        mPrefs.edit().putInt(
                PrefsSingleton.HERO_TYPE,
                1
        ).apply();

        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    public void chooseTrin(View view) {
        if (!trinity) return;
        Sound.playParticularSound(this, R.raw.choose_trinity);
        mPrefs.edit().putInt(
                PrefsSingleton.HERO_TYPE,
                2
        ).apply();

        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.start(this, Music.MUSIC_MENU);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.pause();
    }

}
