package com.livermor.impulse.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.jawnnypoo.physicslayout.Physics;
import com.jawnnypoo.physicslayout.PhysicsRelativeLayout;
import com.livermor.impulse.R;
import com.livermor.impulse.data.SessionSingleton;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.managers.Music;
import com.livermor.impulse.managers.ShareResult;
import com.livermor.impulse.managers.Sound;
import com.livermor.impulse.managers.Units;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GameActivity extends AppCompatActivity
        implements Gamer {

    private static final String TAG = GameActivity.class.getSimpleName();

    @Bind(R.id.physics_layout_game)
    PhysicsRelativeLayout mPhysicsLayout;
    @Bind(R.id.current_round_id)
    TextView mCurrentRoundText;
    @Bind(R.id.record_id)
    TextView mRecordText;

    Units mUnits;
    SessionSingleton mSession;
    Handler gameWatcher;

    SharedPreferences mPrefs;
    private CallbackManager mCallbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        ButterKnife.bind(this);
        mPrefs = PrefsSingleton.getSharedPrefs();
        mRecordText.setText("Record: "
                        + String.valueOf(
                        mPrefs.getInt(PrefsSingleton.RECORD, 1)
                )
        );

        mCallbackManager = CallbackManager.Factory.create();//without it app will crush by cancelling share

        //populate mUnits manager
        final Rect mArea = new Rect();

        mPhysicsLayout.getHitRect(mArea);
        mUnits = new Units(this, mPhysicsLayout, mArea);

        mSession = SessionSingleton.getInstance();

        //touch everywhere to
        mPhysicsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                //return super.onTouchEvent(event);

                if (e.getAction() == MotionEvent.ACTION_DOWN) {
                    impulseHero(e.getX() / 100, e.getY() / 100);
                }
                return true;
            }
        });

        //allow gravity and motion
        mPhysicsLayout.getPhysics().enablePhysics();

        mPhysicsLayout.setGravity((int) Physics.MOON_GRAVITY);

        //create first level
        mSession.firstLevel(mPhysicsLayout, mUnits);


        //watching for win/loss indicators
        gameWatcher = new Handler();
        gameWatcher.post(new Runnable() {
            @Override
            public void run() {
                //if win — show Dialog, install settings and start next lever
                if (mSession.getCurrentLevel() == mSession.getEnemiesKilled() ||
                        mPhysicsLayout.getChildCount() <= 1
                        ) {

                    Sound.playParticularSound(GameActivity.this, R.raw.win_sound);

                    mSession.nextLevel(mPhysicsLayout, mUnits);
                    mCurrentRoundText.setText("Level: " + mSession.getCurrentLevel());

                } else if (mSession.heroIsDead()) {

                    Sound.playLostSound(GameActivity.this);
                    userLost();

                    mSession.firstLevel(mPhysicsLayout, mUnits);
                    mCurrentRoundText.setText("Level: " + mSession.getCurrentLevel());
                }

                //Log.i(TAG, "gameWatcher is working");

                gameWatcher.postDelayed(this, 1200);
            }
        });
    }

    private void impulseHero(float x, float y) {
        mSession.heroAttack(mPhysicsLayout, x, y);
    }

    public void onPauseButton(View v) {
        if (mPhysicsLayout.getPhysics().isPhysicsEnabled()) {
            pause();
        } else {
            resume();
        }
    }

    private void userLost() {

        pause();

        if (mPrefs.getInt(PrefsSingleton.RECORD, 1) < mSession.getCurrentLevel()) {

            mPrefs.edit().putInt(
                    PrefsSingleton.RECORD,
                    mSession.getCurrentLevel()).apply();
            mRecordText.setText(
                    "Record: " + String.valueOf(mSession.getCurrentLevel())
            );

            //****************************************************************
            //share dialog with inner interface;
            String shareText = "My record is " +
                    String.valueOf(mSession.getCurrentLevel());
            ShareResult.showDialog(
                    GameActivity.this,
                    (String) getText(R.string.share_title_basic),
                    shareText);

            if (mSession.getCurrentLevel() == 17) {
                Toast toast = Toast.makeText(this, "Trinity is available", Toast.LENGTH_LONG);
                toast.show();
            } else if (mSession.getCurrentLevel() == 11) {
                Toast toast = Toast.makeText(this, "Morpheus is available", Toast.LENGTH_LONG);
                toast.show();
            }

        } else {

            final AlertDialog.Builder builder = new AlertDialog.Builder(
                    GameActivity.this,
                    R.style.my_alert_dialog_style
            );
            builder.setCancelable(false);
            builder.setTitle(getString(R.string.lost_inform));

            builder.setPositiveButton("reload!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    resume();
                    dialog.dismiss();
                }
            });
            builder.show();
        }
    }

    @Override //Gamer interface
    public void pause() {
        if (mPhysicsLayout.getPhysics().isPhysicsEnabled()) {
            mPhysicsLayout.getPhysics().disablePhysics();
            Music.pause();
        }
    }

    @Override //Gamer interface
    public void resume() {
        if (!mPhysicsLayout.getPhysics().isPhysicsEnabled()) {
            mPhysicsLayout.getPhysics().enablePhysics();
            Music.start(this, Music.MUSIC_GAME);
        }

    }

    @Override //Gamer interface
    public void setRecordText() {
        mRecordText.setText(
                "Record: " + String.valueOf(mSession.getCurrentLevel())
        );
    }

    @Override //Gamer interface
    public void setLevelText(String level) {
        mCurrentRoundText.setText(level);
    }


    public void onSoundChange(View view) {
        boolean soundOn = mPrefs
                .getBoolean(PrefsSingleton.SOUND_ON, true);

        if (soundOn) {
            mPrefs.edit().putBoolean(PrefsSingleton.SOUND_ON, false).apply();
            Music.pause();
        } else {
            mPrefs.edit().putBoolean(PrefsSingleton.SOUND_ON, true).apply();
            Music.start(this, Music.MUSIC_GAME);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.start(this, Music.MUSIC_GAME);
    }

    @Override
    public void onBackPressed() {


        pause();
        final AlertDialog.Builder builder = new AlertDialog.Builder(
                GameActivity.this,
                R.style.my_alert_dialog_style
        );
        builder.setCancelable(false);
        builder.setTitle("Exit?");

        builder.setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                mSession.setCurrentLevel((byte) -1);
                gameWatcher.removeCallbacksAndMessages(null);
                dialog.dismiss();
                resume();

                Intent intent = new Intent(GameActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        builder.setNegativeButton("No!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resume();
                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override //for fb api
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
