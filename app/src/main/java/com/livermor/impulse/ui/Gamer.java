package com.livermor.impulse.ui;

/**
 * Created by arturdumchev on 05.11.15.
 */
public interface Gamer {

    void pause();

    void resume();

    void setRecordText();

    void setLevelText(String level);
}
