package com.livermor.impulse.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.jpardogo.listbuddies.lib.views.ListBuddiesLayout;
import com.livermor.impulse.R;
import com.livermor.impulse.adapters.CircularAdapter;
import com.livermor.impulse.managers.Music;
import com.livermor.impulse.managers.Popup;
import com.livermor.impulse.managers.Sound;

import java.util.ArrayList;
import java.util.Random;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements ListBuddiesLayout.OnBuddyItemClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private CircularAdapter mAdapterLeft;
    private CircularAdapter mAdapterRight;

    private ArrayList<Integer> mImagesLeft = new ArrayList<Integer>();
    private ArrayList<Integer> mImagesRight = new ArrayList<Integer>();

    private Random mRandom;
    ListBuddiesLayout mListBuddies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);

        mListBuddies = (ListBuddiesLayout) findViewById(R.id.listbuddies);

        getImagesForLeft(mImagesLeft);
        getImagesForRight(mImagesRight);

        mAdapterLeft = new CircularAdapter(this, getResources().getDimensionPixelSize(R.dimen.item_height_small), mImagesLeft);
        mAdapterRight = new CircularAdapter(this, getResources().getDimensionPixelSize(R.dimen.item_height_tall), mImagesRight);
        mListBuddies.setAdapters(mAdapterLeft, mAdapterRight);
        mListBuddies.setOnItemClickListener(this);

        mRandom = new Random();

        Popup.popup(this);
    }

    private void getImagesForLeft(ArrayList<Integer> ar) {
        ar.add(R.drawable.neo_and_morph_bg);
        ar.add(R.drawable.start_game_bg); //1 - game
        ar.add(R.drawable.matrix_bg);
        ar.add(R.drawable.neo_bg_second);
        ar.add(R.drawable.start_train_bg); //4 - train
        ar.add(R.drawable.neo_bg);
        ar.add(R.drawable.start_game_bg); //6 - game
        ar.add(R.drawable.agent_smith_big);
        ar.add(R.drawable.choose_bg); //8 — chose player
        ar.add(R.drawable.thunder_bg);
        ar.add(R.drawable.agent_smith_big);
    }

    private void getImagesForRight(ArrayList<Integer> ar) {
        ar.add(R.drawable.three_bg);
        ar.add(R.drawable.start_game_bg); //1 - game
        ar.add(R.drawable.neo_bg_second);
        ar.add(R.drawable.three_bg);
        ar.add(R.drawable.start_train_bg); //4 - train
        ar.add(R.drawable.matrix_bg);
        ar.add(R.drawable.start_game_bg); //6 - game
        ar.add(R.drawable.agent_smith_big);
        ar.add(R.drawable.choose_bg); // 8 — chose player
        ar.add(R.drawable.neo_and_morph_bg);
    }

    @Override
    public void onBuddyItemClicked(
            AdapterView<?> parent,
            View view,
            int buddy,
            int position,
            long id
    ) {

        if (position == 1 || position == 6) {
            Intent intent = new Intent(this, GameActivity.class);
            startActivity(intent);
        } else if (position == 4) {
            Intent intent = new Intent(this, Training.class);
            startActivity(intent);
        } else if (position == 8) {
            Intent intent = new Intent(this, HeroActivity.class);
            startActivity(intent);
        } else {
            Sound.playMenuSound(MainActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.start(this, Music.MUSIC_MENU);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.pause();
    }

    @Override
    public void onBackPressed() {
        Sound.playParticularSound(this, R.raw.lost_goodbue);
        super.onBackPressed();
    }


    //test


}
