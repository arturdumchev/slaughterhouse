package com.livermor.impulse;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.livermor.impulse.data.PrefsSingleton;
import com.livermor.impulse.managers.ShareResult;
import com.livermor.impulse.managers.Sound;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by arturdumchev on 04.11.15.
 */
public class ApplicationClass extends Application {

    private static final String TAG = ApplicationClass.class.getSimpleName();
    private static ApplicationClass myApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
        PrefsSingleton.getInstance();
        Sound.configurateSession();
        FacebookSdk.sdkInitialize(getApplicationContext());



        Log.w(TAG, "pac name: " +  getPackageName());

        //    printHashKey();
    }

    public static ApplicationClass getContext() {
        return myApplication;
    }

    /*void printHashKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.livermor.slaughterhouse",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.w("anal_KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }*/
}
